## FluxCD Sample Setup
### On TAP:
* Install Flux Kustomize and Helm controllers:
```
brew install fluxcd/tap/flux@0.41
flux install -n flux-system --components kustomize-controller --version v0.41.2
flux install -n flux-system --components helm-controller --version v0.41.2
```

* Set up environment variables:
```
source .env
export GITLAB_USER=${DATA_E2E_FLUX_GIT_USER}
export GITLAB_TOKEN=${DATA_E2E_FLUX_GIT_TOKEN}
export GITLAB_PROJECT=mlops-flux-repo # name of the pre-existing GitLab project
export K8s_CLUSTER=mlops #name assigned to the target Kubernetes cluster
```

* Create secret to be used by GitRepositories:
```
kubectl create secret generic mlops-flux-repo-secret \
--from-literal=username=oauth2 \
--from-literal=password=${DATA_E2E_FLUX_GIT_TOKEN} \
-n flux-system
```

* Commit the repository to git:
```
git add .
git commit -m "Committed changes"
git push origin main
```

### On TMC:
After the git repository has been created via the accelerator,
* (Skip if already done) Create a **Repository credential**, including all required fields. Note: 
  * When creating the repository credential, select **Username/Password** as the Credential type.
  * Enter **oauth** and _Your GitLab Oauth Token_ in the **Username** and **Password** fields respectively.
* Create a **Git Repository** under <selected cluster> -> Add-ons -> Git repositories, and fill out all required fields. Note:
  * 